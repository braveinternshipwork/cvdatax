###CV DATA EXTRACTOR (CVDataX)

___

A simple java application to extract specific contact information data from a csv file.

####Getting Started

___

Clone the repository, create a branch and get started.

####Prerequisities

___

Java SDK 1.7 or greater
Gradle 3.1 or later


####Running the tests

___

To run a single test execute:
> gradle test --tests testName


####Built with

___

+ Gradle - Dependency Management


####Versioning

___

We use git for version control.

####Authors

___

+ *Elsie* - Initial work - Brave Venture labs
+ *Linnet* - Initial work - Brave Venture labs
+ *Andrew* - Additional work - Brave Venture labs
+ *WMIK* - Additional work - Brave Venture labs

####License

___

Currently the project is licensed under the MIT License - see the LICENSE.md file for details
However the licensing is subject to change at any moment by the authors.

####Acknowledgments

___

This project builds on top of what was initially done by *Elsie* and *Linnet* by implementing a modular TDD design.