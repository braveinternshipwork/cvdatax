package com.bvl.cvDataX;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class PhoneValidatorTest {

    private PhoneValidator phoneValidator;

    @BeforeClass
    public void initData() {
        phoneValidator = new PhoneValidator();
    }

    @DataProvider
    public Object[][] ValidPhoneProvider() {
        return new Object[][] { { new String[] { "+254720409881",
                "+254-722-389789", "+ 447924590800", "0700-578657",
                "0733878963", "728762287", "+256 75 535 1743",
                "(+254) 704 942 588", "+256 (312) 637 391/2/3/4",
                "+1 208 2502332", "(020)2712640"} } };
    }

    @DataProvider
    public Object[][] InvalidPhoneProvider() {
        return new Object[][] { { new String[] {"50565 – 00200",
                "70565–00200"} } };
    }

    @Test(dataProvider = "ValidPhoneProvider")
    public void ValidPhoneTest(String[] Phone) {

        for (String temp : Phone) {
            boolean valid = phoneValidator.validate(temp);
            System.out.println("Phone is valid : " +temp +"," +valid);
            Assert.assertEquals(valid, true);
        }
    }

    @Test(dataProvider = "InvalidPhoneProvider", dependsOnMethods = "ValidPhoneTest")
    public void InvalidPhoneTest(String[] Phone) {

        for (String temp : Phone) {
            boolean valid = phoneValidator.validate(temp);
            System.out.println("Phone is valid : " +temp +"," +valid);
            Assert.assertEquals(valid, false);
        }
    }
}
