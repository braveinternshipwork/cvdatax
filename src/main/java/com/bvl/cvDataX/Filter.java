package com.bvl.cvDataX;

import com.opencsv.CSVReader;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

class Filter {

    private int _id = 0;
    //private EmailValidator emailValidator = new EmailValidator();
    private PhoneValidator phoneValidator = new PhoneValidator();

    /**
     * Method to check for certain words that prefix email & contact fields
     *
     * @param string
     *                  string for validation
     * @return true string is present, false string is not present
     */
    boolean preFilter(final String string) {
        ArrayList<String> prefixes = new ArrayList<String>();
        prefixes.addAll(Arrays.asList("Email", "E-mail", "Mobile", "Cell", "Tel", "Phone", "Contact"));

        for (String prefix : prefixes) {
            if (string.toLowerCase().startsWith(prefix.toLowerCase())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method to extract relevant data from csv file
     *
     * @param Filename
     */
    void filter(String Filename){

        String[] line, ref_multi, ref_single;

        try {
            CSVReader reader = new CSVReader(new FileReader(Filename));

            while ((line = reader.readNext()) != null) {
                System.out.println(_id +"\n" /*+ line[1] +"\n" + line[3] +"\n"*/ + line[4] +"\n" ); // candidate email

                if (line[5].contains("&&")) {
                    ref_multi = line[5].split("&&");

                    for (String part : ref_multi) {
                        if (part.contains(";")) {
                            ref_single = part.split(";");

                            System.out.println(ref_single[0]); // Referee name

                            for (String _part : ref_single){
                                if (phoneValidator.validate(_part) ||
                                        //emailValidator.validate(_part) ||
                                        preFilter(_part) ||
                                        _part.contains("@")){

                                    System.out.println(_part);
                                }
                                //System.out.println(_part);
                            }
                        }

                        System.out.println("\n");
                    }
                }

                _id +=1;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void postFilter(String string) {
        // TODO: Implement postFilter method to clean data
    }
}
