package com.bvl.cvDataX;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

class PhoneValidator {

    private Pattern pattern;

    private static final String PHONE_PATTERN =
            "^\\s*(?:\\(\\s*\\+|\\+\\s*\\d|\\+\\s*|\\(|254|\\)|020|0|7|\\-|\\s*)"+
                    "+(\\(?\\d\\s*\\-?\\s*\\)?\\s*){8,11}+"+
                    "(?:\\s*\\/\\s*\\d)*";

    PhoneValidator() {
        pattern = Pattern.compile(PHONE_PATTERN);
    }

    /**
     * Validate hex with regex
     *
     * @param hex
     *              hex for validation
     * @return true valid hex, false invalid hex
     */
    boolean validate(final String hex) {
        Matcher matcher = pattern.matcher(hex);
        return matcher.matches();
    }
}
