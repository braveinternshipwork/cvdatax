package com.bvl.cvDataX;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class FilterTest {

    private Filter _filter;

    @BeforeClass
    public void setup() {
        _filter = new Filter();
    }

    @DataProvider
    public Object [][] stringPresent() {
        return new Object[][] { { new String[] { "Email", "Mobile",
                "contact", "CELL", "TeL", "E-MAIL" } } };
    }

    @DataProvider
    public Object[][] stringAbsent() {
        return new Object[][] {{ new String[] { "Any other string"} } };
    }

    @Test(dataProvider = "stringPresent")
    public void preFilterShouldReturnTrueIfPrefixStringIsPresent(String[] strings) {

        for (String temp : strings) {
            boolean present = _filter.preFilter(temp);
            System.out.println("Prefix is present : " +temp +"," +present);
            Assert.assertEquals(present, true);
        }
    }

    @Test(dataProvider = "stringAbsent", dependsOnMethods = "preFilterShouldReturnTrueIfPrefixStringIsPresent")
    public void preFilterShouldReturnFalseIfPrefixStringIsAbsent(String[] strings) {

        for (String temp : strings) {
            boolean present = _filter.preFilter(temp);
            System.out.println("Prefix is present : " +temp +"," +present);
            Assert.assertEquals(present, false);
        }
    }

    @Test
    public void testFilter(){
        // TODO: test filter method
    }

    @Test
    public void testPostFilter() {
        // TODO: test postFilter method
    }
}
