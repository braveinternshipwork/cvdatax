package com.bvl.cvDataX;

import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;

import java.util.logging.Level;
import java.util.logging.Logger;

//import com.mongodb.client.MongoCollection;

public class MongoJDBC {

    public void connect() {

        // Prevents logging in console
        Logger mongoLogger = Logger.getLogger("org.mongodb");
        mongoLogger.setLevel(Level.SEVERE);

        try {

            // Create new connection to database
            MongoClient mongoClient = new MongoClient("localhost", 27017);
            MongoIterable<String> databases, collections; // Iterable to hold databases and collections

            databases = mongoClient.listDatabaseNames();

            // Display databases
            for (String database : databases) {
                System.out.println(database);
            }

            // Specify database to view collections
            MongoDatabase database = mongoClient.getDatabase("");

            collections = database.listCollectionNames();

            // Display collections
            for (String collection : collections) {
                System.out.println(collection);
            }

        } catch (MongoException e) {
            e.printStackTrace();
        }
    }
}