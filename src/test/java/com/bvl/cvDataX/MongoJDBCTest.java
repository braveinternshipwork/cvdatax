package com.bvl.cvDataX;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MongoJDBCTest {

    @BeforeMethod
    public void preventLogging() {

        // Prevents logging in console
        Logger mongoLogger = Logger.getLogger("org.mongodb");
        mongoLogger.setLevel(Level.SEVERE);
    }

    @Test
    public void shouldCreateNewMongoClientConnectedToLocalhost() throws Exception {

        // When
        // DONE: get/create the MongoClient

        MongoClient mongoClient = new MongoClient();

        Assert.assertNotNull(mongoClient);
    }

    @Test
    public void shouldGetDatabaseFromMongoClient() throws Exception {

        // Given
        // DONE: any setup

        MongoClient mongoClient = new MongoClient();

        // When
        // DONE: get the database from the client

        MongoDatabase database = mongoClient.getDatabase("test");

        // Then
        Assert.assertNotNull(database);
    }

    @Test
    public void shouldGetCollectionFromDatabase() throws Exception {

        // Given
        // DONE: any setup
        MongoClient mongoClient = new MongoClient();
        MongoDatabase db = mongoClient.getDatabase("test");

        // When
        // DONE: get collection

        MongoCollection<Document> collection = db.getCollection("testCol");

        // Then
        Assert.assertNotNull(collection);
    }

    @Test(expectedExceptions = Exception.class)
    public void shouldNotBeAbleToUseMongoClientAfteritHasBeenClosed() throws UnknownHostException {

        // Given
        MongoClient mongoClient = new MongoClient();

        // When
        // DONE: close the mongoclient
        mongoClient.close();

        mongoClient.getDatabase("").getCollection("").insertOne(new Document());
    }
}
