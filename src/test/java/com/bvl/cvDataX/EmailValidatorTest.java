package com.bvl.cvDataX;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class EmailValidatorTest {

    private EmailValidator emailValidator;

    @BeforeClass
    public void initData() {
        emailValidator = new EmailValidator();
    }

    @DataProvider
    public Object[][] ValidEmailProvider() {
        return new Object[][] { { new String[] { "williemik@yahoo.com",
                "williemik-100@yahoo.com", "williemik.100@yahoo.com",
                "williemik111@mik.com", "williemik-100@mik.net",
                "williemik.100@mik.com.ke", "williemik@1.com",
                "williemik@gmail.com.com", "williemik+100@gmail.com",
                "williemik-100@yahoo-test.com"} } };
    }

    @DataProvider
    public Object[][] InvalidEmailProvider() {
        return new Object[][] { { new String[] { "williemik", "williemik@.com.my",
                "williemik123@gmail.a", "williemik123@.com", "williemik123@.com.com",
                ".williemik@mik.com", "williemik()*@gmail.com", "williemik@%*.com",
                "williemik..2002@gmail.com", "williemik.@gmail.com",
                "williemik@wmik@gmail.com", "williemik@gmail.com.1a"} } };
    }

    @Test(dataProvider = "ValidEmailProvider")
    public void ValidEmailTest(String[] Email) {

        for (String temp : Email) {
            boolean valid = emailValidator.validate(temp);
            System.out.println("Email is valid : " +temp +"," +valid);
            Assert.assertEquals(valid, true);
        }
    }

    @Test(dataProvider = "InvalidEmailProvider", dependsOnMethods = "ValidEmailTest")
    public void InvalidEmailTest(String[] Email) {

        for (String temp : Email) {
            boolean valid = emailValidator.validate(temp);
            System.out.println("Email is valid : " +temp +"," +valid);
            Assert.assertEquals(valid, false);
        }
    }

}