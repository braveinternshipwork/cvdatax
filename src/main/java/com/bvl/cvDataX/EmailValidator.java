package com.bvl.cvDataX;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

class EmailValidator {

    private Pattern pattern;

    private static final String EMAIL_PATTERN =
            "^(\\s*[_A-Za-z0-9-\\+\\s*])+(\\.[_A-Za-z0-9-]+)*@"+
                    "[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";

    EmailValidator() {
        pattern = Pattern.compile(EMAIL_PATTERN, Pattern.CASE_INSENSITIVE);
    }

    /**
     * Validate hex with regex
     *
     * @param hex
     *              hex for validation
     * @return true valid hex, false invalid hex
     */
    boolean validate(final String hex) {
        Matcher matcher = pattern.matcher(hex);
        return matcher.matches();
    }
}
